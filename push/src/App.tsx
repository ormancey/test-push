import React from 'react';
import logo from './logo.svg';
import './App.css';
import Register from './Register';

function App() {
   return (
     <div className="App">
       <header className="App-header">
         <img src="images/Logo-Outline-web-Blue100.png" className="App-logo" style={{height: '100px'}} alt="logo" />
         <Register/>
       </header>
     </div>
   );
}

export default App;
