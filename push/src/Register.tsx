import React, { useState, useEffect } from 'react'; // let's also import Component
import { register, unregister } from 'register-service-worker'
import {Button, Checkbox, Popup, Table} from 'semantic-ui-react';


interface IProps {
}

function Register(props: IProps){
    const [status, setStatus] = useState<string[]>([]);
    const [isEnabled, setIsEnabled] = useState(false);

    useEffect( () => {
        // Anything in here is fired on component mount.
        navigator.serviceWorker.getRegistrations().then(function(registrations) {
            if (registrations && registrations.length > 0)
                setIsEnabled(true);
        });
     }, []);

    function registerWorker()
    {
        Notification.requestPermission(function(result) {
            console.log("Push Permission User choice: ", result);
            if (result !== "granted") 
            {
              console.log("No notification permission granted!");
              setStatus([...status, 'No notification permission granted!'] );
            } 
            else 
            {
                register('/service.js', {
                    registrationOptions: { scope: './' },
                    ready (registration) {
                        console.log('Service worker is active.');
                        console.log(registration);
                        setStatus((prevState) => ([...prevState, 'Service worker is active.']));

                        // This will be called only once when the service worker is installed for first time.
                        try {
                            const applicationServerKey = urlB64ToUint8Array(
                                'BDC_Z4KiJzs0II7qLJT3DvlVJ0qcWkCMA6u7Q8B1QnpJeax5kShz6-PjoTM7HlTJua1qlXVGLbiZRT3SBM7ZzaY'
                            )
                            const subscriptionOptions = { applicationServerKey, userVisibleOnly: true };
                            registration.pushManager.subscribe(subscriptionOptions)
                                .then(subscription => {
                                    //const response = await saveSubscription(subscription);
                                    console.log('subscribing from caller tsx');
                                    console.log(JSON.stringify(subscription));
                                });
                        } catch (err) {
                            console.log('Error', err)
                        }
                        
                    },
                    registered (registration) {
                        console.log('Service worker has been registered.');
                        console.log(registration);
                        setStatus((prevState) => ([...prevState, 'Service worker has been registered.']));
                        setIsEnabled(true);
                    },
                    cached (registration) {
                        console.log('Content has been cached for offline use.');
                        setStatus((prevState) => ([...prevState, 'Content has been cached for offline use.']));
                    },
                    updatefound (registration) {
                        console.log('New content is downloading.');
                        setStatus((prevState) => ([...prevState, 'New content is downloading.']));
                    },
                    updated (registration) {
                        console.log('New content is available; please refresh.');
                        setStatus((prevState) => ([...prevState, 'New content is available; please refresh.']));
                    },
                    offline () {
                        console.log('No internet connection found. App is running in offline mode.');
                        setStatus((prevState) => ([...prevState, 'No internet connection found. App is running in offline mode.']));
                    },
                    error (error) {
                        console.error('Error during service worker registration:', error);
                        setStatus((prevState) => ([...prevState, 'Error during service worker registration:' + error.message]));
                    }
                });
            }
        });
    }

    function unRegisterWorker()
    {
        unregister();
        setIsEnabled(false);

        /*        navigator.serviceWorker.getRegistrations().then(function(registrations) {
            for(let registration of registrations) {
              registration.unregister()
              console.log("one service worker removed");

              setIsEnabled(false);
            } 
          });
      */
        console.log('service worker unregistered');
        console.log(status);
        setStatus((prevState) => ([...prevState, 'service worker unregistered']));
    }

    // urlB64ToUint8Array is a magic function that will encode the base64 public key
    // to Array buffer which is needed by the subscription option
    function urlB64ToUint8Array(base64String: string | any[]) {
        const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
        const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/')
        const rawData = atob(base64)
        const outputArray = new Uint8Array(rawData.length)
        for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i)
        }
        return outputArray
    }

  
    function toggleRegistration()
    {
        if (isEnabled)
            unRegisterWorker();
        else
            registerWorker();
    }

    return (
        <div>
            <h1>Register for Push Notifications</h1>
            <div>
                <Popup
                    trigger={
                    <Checkbox
                        checked={isEnabled}
                        onChange={toggleRegistration}
                        toggle
                    />
                    }
                    content="Enable Push Notifications"
                    position="right center"
                />
            </div>
            {/* <div style={{marginTop: '10px'}}>
                <button onClick={registerWorker}>Register</button>
                <button onClick={unRegisterWorker}>Unregister</button>
            </div> */}
            <div style={{marginTop: '10px'}}>
                Status:
                {
                    status.map((str, i) => (
                        <div style={{fontSize: 'small'}} key={i}>{str} <br /></div>
                    ))
                }
            </div>
        </div>
    );
}

export default Register;