// urlB64ToUint8Array is a magic function that will encode the base64 public key
// to Array buffer which is needed by the subscription option
const urlB64ToUint8Array = base64String => {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
  const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/')
  const rawData = atob(base64)
  const outputArray = new Uint8Array(rawData.length)
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
}

const saveSubscription = async subscription => {
  const SERVER_URL = 'https://send-push-test.app.cern.ch/save-subscription'
  const response = await fetch(SERVER_URL, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(subscription),
  })
  return response.json()
}

/*self.addEventListener('activate', async () => {
  // This will be called only once when the service worker is installed for first time.
  try {
    const applicationServerKey = urlB64ToUint8Array(
      'BDC_Z4KiJzs0II7qLJT3DvlVJ0qcWkCMA6u7Q8B1QnpJeax5kShz6-PjoTM7HlTJua1qlXVGLbiZRT3SBM7ZzaY'
    )
    const options = { applicationServerKey, userVisibleOnly: true }
    const subscription = await self.registration.pushManager.subscribe(options)
    const response = await saveSubscription(subscription)
    console.log(response)
  } catch (err) {
    console.log('Error', err)
  }
})*/

self.addEventListener('push', async function(event) {
  if (event.data) {
    console.log('Push event text: ', event.data.text());
    console.log(event.data);

    var options = {
      //body: str,
      icon: '/images/Logo-Outline-web-Blue100.png',
      badge: '/images/Logo-Outline-web-Blue100.png',
    //  image: '/images/pic.jpg',
      actions: [
        {
          action: 'yes-action',
          title: 'Yes',
          //icon: '/images/action-1-128x128.png'
        },
        {
          action: 'no-action',
          title: 'No',
          //icon: '/images/action-2-128x128.png'
        }
      ]
    };

    try {
      const blob = JSON.parse(event.data.text());
      options.body = blob.message;
      options.image = blob.image;
      if (blob.url)
        options.data = { url: blob.url};
    }
    catch {}
    
    // fallback is no json blob in message
    if (!options.body)
      options.body = event.data.text();

    // 2 Actions Max
    //return showLocalNotification('Yolo', event.data.text(), self.registration)
    event.waitUntil(
      self.registration.showNotification('Yola', options)
    );
  } else {
    console.log('Push event but no data');
  }
})

self.addEventListener('notificationclick', function(event) {
  if (event.notification.data && event.notification.data.url)
  {
    event.notification.close();
    event.waitUntil(
      clients.openWindow(event.notification.data.url)
    );
  } else
    console.log('Url undefined.');
})

const showLocalNotification = (title, body, swRegistration) => {
  const options = {
    body,
    // here you can add more properties like icon, image, vibrate, etc.
  }
  return swRegistration.showNotification(title, options)
}

