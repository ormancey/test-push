

var safariOn = function () {
    log("Safari starting");

    // Ensure that the user can receive Safari Push Notifications.
    if ('safari' in window && 'pushNotification' in window.safari) {
        var permissionData = window.safari.pushNotification.permission('web.ch.cern.notifications');
        checkRemotePermission(permissionData);
    } else {
        log('not safari or no push support in this version');
    }
}
var safariOff = function () {
    log("safariOff, not implemented");
}

var checkRemotePermission = function (permissionData) {
    if (permissionData.permission === 'default') {
        // This is a new web service URL and its validity is unknown.
        window.safari.pushNotification.requestPermission(
            'https://send-push-test.app.cern.ch/apple/safari', // The web service URL.
            'web.ch.cern.notifications',     // The Website Push ID.
            {'anonid': 'anonid-123456-anonid'}, // Data that you choose to send to your server to help you identify the user.
            checkRemotePermission         // The callback function.
        );
    }
    else if (permissionData.permission === 'denied') {
        // The user said no.
        log("the user said no");
    }
    else if (permissionData.permission === 'granted') {
        // The web service URL is a valid push provider, and the user said yes.
        // permissionData.deviceToken is now available to use.
        log("the user said yes");
        log(permissionData.deviceToken);
    }
};

