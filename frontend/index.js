const check = () => {
    if (!('serviceWorker' in navigator)) {
      log("No Service Worker support!");
      throw new Error('No Service Worker support!');
    }
    if (!('PushManager' in window)) {
      log("No Push API Support!");
      throw new Error('No Push API Support!');
    }
    log("Check looks good.");
  }
const registerServiceWorker = async () => {
    log("trying to register Service Worker");

    const swRegistration = await navigator.serviceWorker.register('service.js');
    log("Service Worker registered");
    console.log(swRegistration);
    return swRegistration;
}

  const requestNotificationPermission = async () => {
    log("trying to request permissions");

    const permission = await window.Notification.requestPermission()
    // value of permission can be 'granted', 'default', 'denied'
    // granted: user has accepted the request
    // default: user has dismissed the notification permission popup by clicking on x
    // denied: user has denied the request.
    if (permission !== 'granted') {
      log("Permission not granted for Notification");
      throw new Error('Permission not granted for Notification');
    }
    log("Permission is granted");
  }
  const main = async () => {
    log("starting");

    check()
    const swRegistration = await registerServiceWorker()
    const permission = await requestNotificationPermission()
  }
  // main(); we will not call main in the beginning.

  const log = (str) => {
    var div = document.getElementById('txt');
    div.innerHTML += str + '<br/>';
  }

  // Checking status onload
  window.addEventListener('load', async function() {
    check();
    const permission = await requestNotificationPermission()
  })

  const deleteSubscription = () => {
    log("Removing service worker");
    navigator.serviceWorker.getRegistrations().then(function(registrations) {
      for(let registration of registrations) {
        log("one service worker removed");
        registration.unregister()
      } 
    });
  }
  