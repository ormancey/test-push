// urlB64ToUint8Array is a magic function that will encode the base64 public key
// to Array buffer which is needed by the subscription option
const urlB64ToUint8Array = base64String => {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
  const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/')
  const rawData = atob(base64)
  const outputArray = new Uint8Array(rawData.length)
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
}

const saveSubscription = async subscription => {
  const SERVER_URL = 'https://send-push-test.app.cern.ch/save-subscription'
  const response = await fetch(SERVER_URL, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(subscription),
  })
  return response.json()
}

self.addEventListener('activate', async () => {
  // This will be called only once when the service worker is installed for first time.
  try {
    const applicationServerKey = urlB64ToUint8Array(
      'BDC_Z4KiJzs0II7qLJT3DvlVJ0qcWkCMA6u7Q8B1QnpJeax5kShz6-PjoTM7HlTJua1qlXVGLbiZRT3SBM7ZzaY'
    )
    const options = { applicationServerKey, userVisibleOnly: true }
    const subscription = await self.registration.pushManager.subscribe(options)
    const response = await saveSubscription(subscription)
    console.log(response)
  } catch (err) {
    console.log('Error', err)
  }
})

self.addEventListener('push', async function(event) {
  if (event.data) {
    console.log('Push event!! ', event.data.text())
    //return showLocalNotification('Yolo', event.data.text(), self.registration)
    event.waitUntil(
      self.registration.showNotification('Yola', {
        body: event.data.text(),
        
        icon: '/images/cern.512x512.png',
        badge: '/images/cern.128x128.png',
        image: '/images/pic.jpg',
        actions: [
          {
            action: 'coffee-action',
            title: 'Coffee',
            icon: '/images/action-1-128x128.png'
          },
          {
            action: 'doughnut-action',
            title: 'Doughnut',
            icon: '/images/action-2-128x128.png'
          }
        ]
      })
    );
  } else {
    console.log('Push event but no data')
  }
})

const showLocalNotification = (title, body, swRegistration) => {
  const options = {
    body,
    // here you can add more properties like icon, image, vibrate, etc.
  }
  return swRegistration.showNotification(title, options)
}

