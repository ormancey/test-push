FROM node:12

RUN apt update && apt install -y curl vim

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY frontend/ frontend/
COPY backend/ backend/
#COPY push/ push/

COPY startup.sh .
RUN chmod a+x startup.sh

RUN cd backend && npm install && cd ..
RUN cd frontend && npm install && cd ..
#RUN cd push && yarn install && yarn build && yarn global add serve &&  cd ..
#RUN cd push && yarn install cd ..
RUN npm install -g http-server
RUN chmod -R 777 *

EXPOSE 8080 4000
ENTRYPOINT [ "/usr/src/app/startup.sh" ]

