const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const webpush = require('web-push')
const fs = require('fs')
const path = require('path');
const app = express()
app.use(cors())
app.use(bodyParser.json())
const port = 4000
app.get('/', (req, res) => res.send('Hello World!'))
const dummyDb = { subscription: null } //dummy in memory store
var hashSubscriptions = {}; // store in memory as an array

try {
  hashSubscriptions = require('/usr/src/app/backend/data/subscriptions.json');
} catch ( err ) {
   // noting the array is already initialized
}

const saveToDatabase = async subscription => {
  // Since this is a demo app, I am going to save this in a dummy in memory store. Do not do this in your apps.
  // Here you should be writing your db logic to save it.
  //dummyDb.subscription = subscription
  hashSubscriptions[subscription.endpoint] = subscription;
  //
  console.log("Current subscriptions"); 
  for (var endpoint in hashSubscriptions)
    console.log(endpoint);
  // Should save to file or something here
  require('fs').writeFile(
    '/usr/src/app/backend/data/subscriptions.json',
    JSON.stringify(hashSubscriptions),
    function (err) {
        if (err) {
            console.error('failed to save subsbriptions array to file: ' + err);
        }
    }
  );
}

// The new /save-subscription endpoint
app.post('/save-subscription', async (req, res) => {
  const subscription = req.body;
  await saveToDatabase(subscription); //Method to save the subscription to Database
  console.log(subscription);
  res.json({ message: 'success' });
})

const vapidKeys = {
  publicKey:
    'BDC_Z4KiJzs0II7qLJT3DvlVJ0qcWkCMA6u7Q8B1QnpJeax5kShz6-PjoTM7HlTJua1qlXVGLbiZRT3SBM7ZzaY',
  privateKey: 'bUQFeijw-pda6DlNMHrZDQYq_VRIctKZiRSzUILlXfM',
}
//setting our previously generated VAPID keys
webpush.setVapidDetails(
  'mailto:myuserid@email.com',
  vapidKeys.publicKey,
  vapidKeys.privateKey
)
//function to send the notification to the subscribed device
const sendNotification = (subscription, dataToSend) => {
  webpush.sendNotification(subscription, dataToSend).catch(error => {
    if (error.statusCode === 410)
      console.log("CLEANUP Needed: " + error.body + " " + error.endpoint);
    else
      console.error(error);
  });
}
//route to test send notification
app.get('/send-notification', (req, res) => {
  
  const message = 'Simple piece of body text.\nSecond line of body text :)';

  const blob = JSON.stringify({
    message: message,
    url: 'http://home.cern',
    image: 'https://cds.cern.ch/images/CMS-PHO-GEN-2008-026-1/file?size=large',
  })

//  const subscription = dummyDb.subscription //get subscription from your databse here.
  for (var endpoint in hashSubscriptions)
  {
    console.log("Sending to: " + endpoint);
    sendNotification(hashSubscriptions[endpoint], blob);
  }

  res.json({ message: 'messages sent' });

})

// --- Safari Specifics endpoints
// https://developer.apple.com/library/archive/documentation/NetworkingInternet/Conceptual/NotificationProgrammingGuideForWebsites/PushNotifications/PushNotifications.html#//apple_ref/doc/uid/TP40013225-CH3-SW9

// Downloading Your Website Package
// webServiceURL/version/pushPackages/websitePushID
app.post('/apple/safari/:version/pushPackages/:websitePushID', async (req, res) => {
  console.log("Downloading Your Website Package");
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  console.log("Connected from: " + ip);
  console.log("Header authorization: " + req.header('authorization'));
  console.log("Params version: " + req.params.version);
  console.log("Params websitePushID: " + req.params.websitePushID);
  const subscription = req.body;
  console.log("Subscription: " + JSON.stringify(subscription));

  res.set({
    'Content-Type': 'application/zip',
    'Content-Disposition': 'attachment; filename="pushPackage.zip"'
  })
  var readStream = fs.createReadStream(path.join(__dirname, 'pushPackage.zip'));
  readStream.pipe(res);
})

// Registering or Updating Device Permission Policy
// webServiceURL/version/devices/deviceToken/registrations/websitePushID
app.post('/apple/safari/:version/devices/:deviceToken/registrations/:websitePushID', async (req, res) => {
  console.log("Registering or Updating Device Permission Policy");
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  console.log("Connected from: " + ip);
  console.log("Header authorization: " + req.header('authorization'));
  console.log("Params version: " + req.params.version);
  console.log("Params deviceToken: " + req.params.deviceToken);
  console.log("Params websitePushID: " + req.params.websitePushID);
  const subscription = req.body;
  console.log("Subscription: " + JSON.stringify(subscription));

  res.json({ message: 'success' });
})

// Forgetting Device Permission Policy
// webServiceURL/version/devices/deviceToken/registrations/websitePushID
app.delete('/apple/safari/:version/devices/:deviceToken/registrations/:websitePushID', async (req, res) => {
  console.log("Forgetting Device Permission Policy");
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  console.log("Connected from: " + ip);
  console.log("Header authorization: " + req.header('authorization'));
  console.log("Params version: " + req.params.version);
  console.log("Params deviceToken: " + req.params.deviceToken);
  console.log("Params websitePushID: " + req.params.websitePushID);
  const subscription = req.body;
  console.log("Subscription: " + JSON.stringify(subscription));

  res.json({ message: 'success' });
})

// Logging Errors
// webServiceURL/version/log
app.post('/apple/safari/:version/log', async (req, res) => {
  console.log("Logging Errors");
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  console.log("Connected from: " + ip);
  console.log("Header authorization: " + req.header('authorization'));
  console.log("Params version: " + req.params.version);
  const subscription = req.body;
  console.log("Safari WS Error: " + JSON.stringify(subscription));

  res.json({ message: 'success' });
})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))